//
//  TERMainView.swift
//  TravelExchangeRate
//
//  Created by Jaxun on 2015/10/7.
//  Copyright © 2015年 Jaxun. All rights reserved.
//

import UIKit
import PKRevealController
import RealmSwift

private let shareInstance = TERMainView()

class TERMainView: UIViewController {
    
    private var accountBookDataSource = TERAccountBookDataSource()
    
    var tag = 0
    var tmpHvalue = CGFloat()
    var mainAccountBook: RLMAccountBook!
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var extendableView: UIView!
    @IBOutlet weak var accountBookCollectionView: UICollectionView?
    
    
    // 目前帳本資訊
    @IBOutlet weak var coverImg: UIImageView!
    @IBOutlet weak var coverTravelName: UILabel?
    @IBOutlet weak var coverTravelDay: UILabel? // 日期需要多另外做處理...
    @IBOutlet weak var coverTravelCountry: UILabel?
    @IBOutlet weak var coverTravelMoney: UILabel?
    @IBOutlet weak var coverTravelStatus: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //// mock value!!
        let userDefault = NSUserDefaults.standardUserDefaults()
        userDefault.setObject("888", forKey: "totalSpendMoney")
        
        // whose view is not in the window hierarchy
        // http://stackoverflow.com/questions/11862883/whose-view-is-not-in-the-window-hierarchy
        /*
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"checkTravelStatus", name: "CheckTravelStatus", object: nil)
        */
        struct Static {
            static var onceToken : dispatch_once_t = 0
            static var staticInstance : TERMainView? = nil
        }
        
        dispatch_once(&Static.onceToken) {
            Static.staticInstance = TERMainView()
            
            delay(0.3, closure: { () -> () in
                self.checkTravelStatus()
            })
        }
    }
    
    // 點擊目前帳本照片
    func tapCurrentAccountBook() {
        print("tap!!")
        self.presentAccountListWithAccountBookId(mainAccountBook)
    }
    override func viewWillAppear(animated: Bool) {
        

    }
    
    override func viewDidAppear(animated: Bool) {

        // MARK: mainAccountBook is nil status 還未處理
        if accountBookDataSource.count > 0 {
            mainAccountBook = accountBookDataSource.accountForItemAtIndex(accountBookDataSource.count)!
            coverImg.image = UIImage(data: (mainAccountBook.accountPhoto)!)
            
            coverTravelName?.text = mainAccountBook.travelName
            //        coverTravelDay?.text = mainAccountBook?.
            coverTravelCountry?.text = mainAccountBook.countryName // 還要存國家
            coverTravelMoney?.text = mainAccountBook.allMoney
            coverTravelStatus!.text = mainAccountBook.travelStatus // 轉中文
            
            let tapGesture = UITapGestureRecognizer(target: self, action: Selector("tapCurrentAccountBook"))
            coverImg.userInteractionEnabled = true
            if coverImg.image != nil { // MARK: or 非預設圖片
                coverImg.addGestureRecognizer(tapGesture)
            }
        }
    }
    
    func checkTravelStatus() {
        // 取得目前旅行狀態
        let travelStatus = NSUserDefaults.standardUserDefaults().objectForKey("travelStatus")
        
        // "旅行中" 直接到計算機頁面
        if travelStatus != nil {
            let tString = travelStatus as? String
            if tString == "Traveling" {
                print("go to calculator view")
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let calculaterVC = storyboard.instantiateViewControllerWithIdentifier("CalculaterView") as! TERCalculaterView
                let sideMenuVC   = storyboard.instantiateViewControllerWithIdentifier("SideMenu") as! TERSideMenu
                
                let revealController = PKRevealController(frontViewController: calculaterVC, leftViewController: sideMenuVC)
                
                self.presentViewController(revealController, animated: false, completion: nil)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func addAccountBook(sender: UIButton) {

        let userDefault = NSUserDefaults.standardUserDefaults()
        
        // 第一次＆旅行結束,進入設定頁 ("旅行中" 不能進設定頁創建帳本)
        if userDefault.objectForKey("travelStatus") != nil {
            let tString = userDefault.objectForKey("travelStatus") as? String
            if tString != "Traveling" {
                
                // 設定帳本狀態"出發前"
                userDefault.setObject("BeforeTravel", forKey: "travelStatus")
                self.performSegueWithIdentifier("MainToPresetting", sender: self)
            } else {
                // show alert to tell user can't add account book.
            }
        } else {

            userDefault.setObject("BeforeTravel", forKey: "travelStatus")
            self.performSegueWithIdentifier("MainToPresetting", sender: self)
        }
    }

    @IBAction func letAccountBookViewFull(sender: UIButton) {
        if self.tag == 0 {
            
            tmpHvalue = self.heightConstraint.constant
            
            self.heightConstraint.constant = self.view.frame.height - 60// ＋ 按鈕的高
            self.tag = 1
        } else {
            self.heightConstraint.constant = tmpHvalue
            self.tag = 0
        }
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "MainToPresetting" {
            
        }
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}

// MARK: UICollectionViewDataSource
extension TERMainView: UICollectionViewDataSource {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //        return accountDataSource.numberOfPapersInSection(section)
        return accountBookDataSource.count - 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("PaperCell", forIndexPath: indexPath) as! PaperCell
        
        if let accountBook = accountBookDataSource.accountForItemAtIndexPath(indexPath) {
            
            cell.paperImageView.image = UIImage(named: "Checked") // 先給預設圖
            
            if accountBook.accountPhoto != nil {
                let photoImg = UIImage(data: accountBook.accountPhoto!)
                cell.paperImageView.image = photoImg
            }
            cell.captionLabel.text = accountBook.travelName
            cell.captionLabel.textColor = UIColor.redColor()
            
        }
        return cell
    }
}

extension TERMainView: UICollectionViewDelegate {
    
    // MARK: UICollectionViewDelegate
    
    // 點選某一旅行後帳本
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        if let accountBook = accountBookDataSource.accountForItemAtIndexPath(indexPath) {
            
            print("\(accountBook)")
            
            self.presentAccountListWithAccountBookId(accountBook)
        }
    }
    
    func presentAccountListWithAccountBookId(accountBook: RLMAccountBook) {
        // change account Book ID
        let aId = Int(accountBook.id)
        print("aid = \(aId)")
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setObject(aId, forKey: "bookIdCount")
        userDefaults.setObject(accountBook.travelStatus, forKey: "TravelStatus")
        userDefaults.synchronize()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let calculaterVC = storyboard.instantiateViewControllerWithIdentifier("AccountList") as! TERAccountList
        let sideMenuVC   = storyboard.instantiateViewControllerWithIdentifier("SideMenu") as! TERSideMenu
        
        let revealController = PKRevealController(frontViewController: calculaterVC, leftViewController: sideMenuVC)
        
        self.presentViewController(revealController, animated: false, completion: nil)
    }
}
