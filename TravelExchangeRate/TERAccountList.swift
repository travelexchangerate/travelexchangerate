//
//  TERAccountList.swift
//  TravelExchangeRate
//
//  Created by Jaxun on 2015/10/8.
//  Copyright © 2015年 Jaxun. All rights reserved.
//

import UIKit

class TERAccountList: UIViewController {
    
    private var accountDataSource = TERAccountsDataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // 點擊側邊menu按鈕
    @IBAction func clickSideMenuBtn(sender: UIButton) {
        if self.revealController != nil {
            self.revealController.showViewController(self.revealController.leftViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func addAccountBook(sender: UIButton) {
        self.performSegueWithIdentifier("AccountListToAccountDetail", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "AccountListToAccountDetail" {

        }

        if segue.identifier == "MasterToDetail" {
            //get destination controller
            let accountDetail = segue.destinationViewController as! TERAccountDetail;
            
            let account = sender as! RLMAccount
            
            accountDetail.account = account
        }
    }
}

extension TERAccountList: UICollectionViewDataSource {
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return accountDataSource.numberOfSections
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return accountDataSource.numberOfPapersInSection(section)
        return accountDataSource.numberOfAccountsInSection(section)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("PaperCell", forIndexPath: indexPath) as! PaperCell
        
        if let account = accountDataSource.accountForItemAtIndexPath(indexPath) {
            
            cell.paperImageView.image = UIImage(named: "Checked") // 先給預設圖
            
            if account.photo1 != nil {
                let photoImg = UIImage(data: account.photo1!)
                cell.paperImageView.image = photoImg
            }
            cell.captionLabel.text = account.accountName
            cell.captionLabel.textColor = UIColor.redColor()
            
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        let sectionHeaderView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "SectionHeader", forIndexPath: indexPath) as! SectionHeaderView
        if let title = accountDataSource.titleForSectionAtIndexPath(indexPath) {
            sectionHeaderView.title = title
            sectionHeaderView.titleLabel.text = title
            sectionHeaderView.titleLabel.textColor = UIColor.whiteColor()
//            sectionHeaderView.icon = UIImage(named: title)
        }
        return sectionHeaderView
    }
}

extension TERAccountList: UICollectionViewDelegate {
 
    // MARK: UICollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
      
        if let account = accountDataSource.accountForItemAtIndexPath(indexPath) {
            
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let accountDetailVC = storyboard.instantiateViewControllerWithIdentifier("AccountDetail") as! TERAccountDetail
            print("\(account)")
            
            performSegueWithIdentifier("MasterToDetail", sender: account)
            
//            let img1 = UIImage(data: account.photo1!)
//            accountDetailVC.photo1 = img1!
            
            //                        accountDetailVC.photo2 = UIImage(data: account.photo2!)
            //                        accountDetailVC.photo3 = UIImage(data: account.photo3!)
            //                        accountDetailVC.photo4 = UIImage(data: account.photo4!)
        }
    }
}
