//
//  TERKeyinView.swift
//  TravelExchangeRate
//
//  Created by Jaxun on 2015/10/7.
//  Copyright © 2015年 Jaxun. All rights reserved.
//

import UIKit
import PKRevealController
import MobileCoreServices
import AssetsLibrary
import Photos
import RealmSwift
import Cosmos

class TERKeyinView: UIViewController {
    
    var spendOrIncome = Bool()                             // 支出/收入 1:支出 0:收入
    var moneyString = ""
    var exchangeMoneyString = ""
    
    var photoChoseTag = Int()                       // 識別選擇了哪個照片
    var imageData1 = NSData()                       // 預存照片的格式
    var imageData2 = NSData()
    var imageData3 = NSData()
    var imageData4 = NSData()
    var location = CLLocationCoordinate2D()         // 座標位置(從LocationView來)
    
    @IBOutlet weak var moneyType: UISegmentedControl!
    @IBOutlet weak var likeRate: CosmosView!
    var feelLevelValue = 3 // 之後要改成Double(可以存0.5)
    
    @IBAction func ChoseMoneyType(sender: AnyObject) {
        if moneyType.selectedSegmentIndex == 0 {
            self.spendOrIncome = true // 支出
        } else  {
            self.spendOrIncome = false // 收入
        }
    }
    @IBOutlet weak var spendTypeLabel: UILabel!     // 現金or信用卡
    @IBOutlet weak var accountNameLabel: UITextField!   // 帳目名稱
    @IBOutlet weak var moneyLabel: UILabel!         // 本國花費錢
    @IBOutlet weak var exchangeMoneyLabel: UILabel! // 出國花費錢
    @IBOutlet weak var spendCategory: UILabel!      // 主要花費類別
    @IBOutlet weak var spendSubCategory: UILabel!   // 次要花費類別
    @IBOutlet weak var currentDay: UILabel!         // 目前日前
    @IBOutlet weak var currentTime: UILabel!        // 目前時間

    @IBOutlet var photoImgView: UIImageView!
    @IBOutlet var photoImgView2: UIImageView!
    @IBOutlet var photoImgView3: UIImageView!
    @IBOutlet var photoImgView4: UIImageView!
    
    var mockName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.moneyLabel.text = moneyString
        self.exchangeMoneyLabel!.text = exchangeMoneyString
        
        // Setting Day & Time 當下的日期＆時間
        let date = NSDate()
        let dateFormatter = NSDateFormatter()
        let timeFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        timeFormatter.dateFormat = "HH:mm"
        let nowDate = dateFormatter.stringFromDate(date)
        let nowTime = timeFormatter.stringFromDate(date)
        
        self.currentDay.text = nowDate
        self.currentTime.text = nowTime
        
        // MARK: 到時候照片名稱要改

        // 預設圖
        self.photoImgView.image = UIImage(named: "Checked")
        let imageTmp = UIImage(named: "Checked")
        self.imageData1 = UIImagePNGRepresentation(imageTmp!)!
        
        let tapGesture = UITapGestureRecognizer(target: self, action: Selector("capture"))
        self.photoImgView.userInteractionEnabled = true
        self.photoImgView.addGestureRecognizer(tapGesture)
        
        
        self.photoImgView2.userInteractionEnabled = true
        let tapGesture2 = UITapGestureRecognizer(target: self, action: Selector("capture2"))
        self.photoImgView2.addGestureRecognizer(tapGesture2)
        
        self.photoImgView3.userInteractionEnabled = true
        let tapGesture3 = UITapGestureRecognizer(target: self, action: Selector("capture3"))
        self.photoImgView3.addGestureRecognizer(tapGesture3)

        self.photoImgView4.userInteractionEnabled = true
        let tapGesture4 = UITapGestureRecognizer(target: self, action: Selector("capture4"))
        self.photoImgView4.addGestureRecognizer(tapGesture4)
        
        print("xxxx\(self.mockName)=======")
        
        // 先給座標點個位置
//        self.location.latitude = 0.0
//        self.location.longitude = 0.0
        
        self.likeRate.didTouchCosmos = didTouchCosmos
        self.likeRate.didFinishTouchingCosmos = didFinishTouchingCosmos
    }
    
    func setDefaultRealmForUser(username: String) {
        var config = Realm.Configuration()
        
        // 使用默认的目录，但是使用用户名来替换默认的文件名
        config.path = NSURL.fileURLWithPath(config.path!)
            .URLByDeletingLastPathComponent?
            .URLByAppendingPathComponent(username)
            .URLByAppendingPathExtension("realm")
            .path
        
        // 将这个配置应用到默认的 Realm 数据库当中
        Realm.Configuration.defaultConfiguration = config
    }
    
    override func viewDidAppear(animated: Bool) {

    }
    
    override func viewWillAppear(animated: Bool) {
        print("got it !!!!!!!!!!!!!!!! \(self.location)")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backToCalculaterView(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: 相機
    @IBAction func capture() {
        self.photoChoseTag = 1
        self.choseCamaraOrAlbum()
    }
    
    @IBAction func capture2() {
        self.photoChoseTag = 2
        self.choseCamaraOrAlbum()
    }
    @IBAction func capture3() {
        self.photoChoseTag = 3
        self.choseCamaraOrAlbum()
    }
    @IBAction func capture4() {
        self.photoChoseTag = 4
        self.choseCamaraOrAlbum()
    }
    
    func choseCamaraOrAlbum() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
            
            // Chose camara or album
            
            let alert: UIAlertController =
            UIAlertController(title: "error", message: nil, preferredStyle: .Alert)
            
            let camaraAction: UIAlertAction = UIAlertAction(title: "open camara", style: .Default) { action -> Void in
                self.openPhotoBy("camara")
            }
            let albumAction: UIAlertAction = UIAlertAction(title: "open from album", style: .Default) { action -> Void in
                self.openPhotoBy("library")
            }
            alert.addAction(camaraAction)
            alert.addAction(albumAction)
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            // Open album
            self.openPhotoBy("library")
        }
    }
    
    // Star-Rating (Cosmos View)
    
    private func didTouchCosmos(rating: Double) {
//        ratingSlider.value = Float(rating)
//        ratingLabel.text = ViewController.formatValue(rating)
//        ratingLabel.textColor = UIColor(red: 133/255, green: 116/255, blue: 154/255, alpha: 1)
        print("didT \(rating)")
    }
    
    private func didFinishTouchingCosmos(rating: Double) {
//        ratingSlider.value = Float(rating)
//        //self.ratingLabel.text = ViewController.formatValue(rating)
//        ratingLabel.textColor = UIColor(red: 183/255, green: 186/255, blue: 204/255, alpha: 1)
        
        print("Finish\(rating)")
        self.feelLevelValue = Int(rating)
    }
}

// MARK: - ImagePickable Protocol
protocol ImagePickable {
    var imagePicker: UIImagePickerController { get set }
    func openPhotoBy(type: String)
}

var AssociatedObjectHandle: UInt8 = 0

extension TERKeyinView: ImagePickable, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var imagePicker:UIImagePickerController {
        get {
            return objc_getAssociatedObject(self, &AssociatedObjectHandle) as! UIImagePickerController
        }
        set {
            objc_setAssociatedObject(self, &AssociatedObjectHandle, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func openPhotoBy(type: String) {
        imagePicker = UIImagePickerController()
        imagePicker.modalPresentationStyle = .CurrentContext
        if type == "camara" {
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera;
        } else {
            imagePicker.sourceType = .PhotoLibrary
        }
        imagePicker.delegate = self
        
        presentViewController(imagePicker, animated: true, completion: nil)
    }
}

// MARK: - Camara Delegate
extension TERKeyinView {
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        print("i've got an image");
        
        var image: UIImage!
        
        // fetch the selected image
        if picker.allowsEditing {
            image = info[UIImagePickerControllerEditedImage] as! UIImage
        } else {
            image = info[UIImagePickerControllerOriginalImage] as! UIImage
        }
        
        let refURL = info[UIImagePickerControllerReferenceURL] as! NSURL
        
        let fetResult = PHAsset.fetchAssetsWithALAssetURLs([refURL], options: nil)
        let asset = fetResult.firstObject as! PHAsset
        asset.creationDate?.description
        asset.location?.description
        asset.location?.coordinate
        print("asset.creationDate?.description =======\(asset.creationDate?.description)")
        print("asset.location?.description ======\(asset.location?.description)")
        print("asset.location?.coordinate ======\(asset.location?.coordinate)")
        
        // Cover UIImage to NSData

        let getPhotoImg = image
        
        switch (photoChoseTag) {
        case 1:
            self.imageData1 = UIImageJPEGRepresentation(image, 50)!
            self.photoImgView.image = getPhotoImg
        case 2:
            self.imageData2 = UIImageJPEGRepresentation(image, 50)!
            self.photoImgView2.image = getPhotoImg
        case 3:
            self.imageData3 = UIImageJPEGRepresentation(image, 50)!
            self.photoImgView3.image = getPhotoImg
        case 4:
            self.imageData4 = UIImageJPEGRepresentation(image, 50)!
            self.photoImgView4.image = getPhotoImg
        default:
            print("error")
        }
        
        // 拍照模式才需要存照片
        if picker.sourceType == UIImagePickerControllerSourceType.Camera {
            UIImageWriteToSavedPhotosAlbum(getPhotoImg, nil, nil, nil)
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

extension TERKeyinView: PKRevealing {
    
    @IBAction func clickFinish(sender: UIButton) {
        
        // save data show alert and chose where to go.
        
        let finishAlertController: UIAlertController =
        UIAlertController(title: "已為您新增至XXX帳目中", message: nil, preferredStyle: .Alert)
        
        let lookingAction: UIAlertAction = UIAlertAction(title: "查看帳目", style: .Default) { action -> Void in
            
            /// MARK: 先從Realm 撈出帳本資料
            let userDefault = NSUserDefaults.standardUserDefaults()
            let tmpId = (userDefault.objectForKey("bookIdCount") as? Int)!
            let accountBookId = String(tmpId)
            
            var accountBook = RLMAccountBook()
            var newAllAccounts = [RLMAccount()]
            
            let realm = try! Realm()
            let resultAccountBook = realm.objects(RLMAccountBook).filter("id == '\(accountBookId)'")
            accountBook = resultAccountBook[0]
            

            // MARK: 在此處新增帳目 (要從帳本id)
            let account = RLMAccount()
            if self.spendOrIncome == true {
                account.spendOrIncome = "收入"
            }
            
            var aIdcount = 0
            if (userDefault.objectForKey("AccountId")) != nil {
                aIdcount = (userDefault.objectForKey("AccountId") as? Int)!
                print("aIdBefore === \(aIdcount)")
            }
                aIdcount++
            userDefault.setObject(aIdcount, forKey: "AccountId")
            userDefault.synchronize()
            print("aIdAfter ===\(aIdcount)")
            account.aId = String(aIdcount)
            account.accountName = self.accountNameLabel.text!//"消費名稱"

            account.price = "999"//self.moneyLabel.text!
            account.exPrice = self.exchangeMoneyLabel.text!
            account.travelDay = self.currentDay.text!
            account.travelTime = self.currentTime.text!
//            account.spendType = self.spendTypeLabel.text! // MARK: 點及切換現金/信用卡還未製作
            account.locationLatitude = self.location.latitude
            account.locationLongitude = self.location.longitude
            
            account.feelLevel = self.feelLevelValue // MARK: 還未製作
            account.category = "早餐"// MARK: 還未製作
            account.ps = "附註： 這個早餐蠻便宜"// MARK: 還未製作
            
            account.photo1 = self.imageData1
            account.photo2 = self.imageData2
            account.photo3 = self.imageData3
            account.photo4 = self.imageData4
            
            account.owner = accountBook
            account.accountBookId = accountBookId // 目前不會使用owner filter的替代方案,希望可拿掉

            newAllAccounts.append(account)
//            newAllAccounts.insert(account, atIndex: 0)
            
            
            try! realm.write({ () -> Void in
                
//                realm.add(account, update: true) // 以帳目為主
                realm.add(account)
                
//                realm.create(RLMAccountBook.self, value: ["id": accountBookId, "accounts": newAllAccounts], update: true)
                // 以帳本為主(目前失敗)
            })
            
            // 推進 帳目頁面＋側邊頁面
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let calculaterVC = storyboard.instantiateViewControllerWithIdentifier("AccountList")
            let sideMenuVC   = storyboard.instantiateViewControllerWithIdentifier("SideMenu")
            
            let revealController = PKRevealController(frontViewController: calculaterVC, leftViewController: sideMenuVC)
            
            self.presentViewController(revealController, animated: false, completion: nil)
        }
        finishAlertController.addAction(lookingAction)
        
        // MARK: 這邊處理流程似乎會更改
        let keepAddAction: UIAlertAction = UIAlertAction(title: "繼續新增", style: .Default) { action -> Void in
            
            // 推進 計算機頁面＋側邊頁面
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let calculaterVC = storyboard.instantiateViewControllerWithIdentifier("CalculaterView")
            let sideMenuVC   = storyboard.instantiateViewControllerWithIdentifier("SideMenu")
            
            let revealController = PKRevealController(frontViewController: calculaterVC, leftViewController: sideMenuVC)
            
            self.presentViewController(revealController, animated: false, completion: nil)
        }
        finishAlertController.addAction(keepAddAction)
        
        self.presentViewController(finishAlertController, animated: true, completion: nil)
    }
    
    @IBAction func clickToLocationView(sender: UIButton) {

        // 先判斷使用者有沒開定位
        if CLLocationManager.locationServicesEnabled() {
            self.performSegueWithIdentifier("KeyinToLocation", sender: self)
        } else {
            let alert: UIAlertController =
            UIAlertController(title: "請先開啟定位功能", message: nil, preferredStyle: .Alert)
            
            let okAction: UIAlertAction = UIAlertAction(title: "OK", style: .Default) { action -> Void in }
            alert.addAction(okAction)
            self.presentViewController(alert, animated: true, completion: nil)
            
            return
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "KeyinToAccountlist" {
            //get destination controller
            //            var destViewController = segue.destinationViewController as! TERPresettingView;
            //            destViewController.receiveData = "SegueData!!!!!";
        }
        if segue.identifier == "KeyinToLocation" {
            let locationVC = segue.destinationViewController as! TERLocationView
            locationVC.keyinVC = self
        }
    }
}
