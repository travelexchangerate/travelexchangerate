//
//  TERAccountsDataSource.swift
//  TravelExchangeRate
//
//  Created by Jaxun on 2015/11/25.
//  Copyright © 2015年 Jaxun. All rights reserved.
//

import UIKit
import RealmSwift

class TERAccountsDataSource {
    
    private var papers: [Paper] = []
    private var immutablePapers: [Paper] = []
    private var sections: [String] = []
    
    private var accounts: [RLMAccount] = []
    private var accountSections: [String] = []
    
//    private var accounts: [Account] = []

    
    var count: Int {
        return accounts.count
//            return papers.count
    }
    
    var numberOfSections: Int {
//        return sections.count
        return self.accountSections.count
    }
    
    init() {
        papers = loadPapersFromDisk()
        
        // get account sections
        //###########
        accounts = loadAccountsFromRealm()
        //##########
    }
    
    /*
    func deleteItemsAtIndexPaths(indexPaths: [NSIndexPath]) {
    var indexes: [Int] = []
    for indexPath in indexPaths {
    indexes.append(absoluteIndexForIndexPath(indexPath))
    }
    var newPapers: [Paper] = []
    //    for (index, paper) in enumerate(papers) {
    //      if !contains(indexes, index) {
    for (index, paper) in papers.enumerate() {
    if !indexes.contains(index) {
    newPapers.append(paper)
    }
    }
    papers = newPapers
    }
    */
    
    /*
    func indexPathForNewRandomPaper() -> NSIndexPath {
    let index = Int(arc4random_uniform(UInt32(immutablePapers.count)))
    let paperToCopy = immutablePapers[index]
    let newPaper = Paper(copying: paperToCopy)
    
    //saveToDisk(newPaper)
    
    papers.append(newPaper)
    papers.sortInPlace { $0.index < $1.index }
    return indexPathForPaper(newPaper)
    }
    */
    
    func indexPathForPaper(paper: Paper) -> NSIndexPath {
        let section = sections.indexOf(paper.section)
        var item = 0
        for (index, currentPaper) in papersForSection(section!).enumerate() {
            if currentPaper === paper {
                item = index
                break
            }
        }
        return NSIndexPath(forItem: item, inSection: section!)
    }
    
    func numberOfPapersInSection(index: Int) -> Int {
        let papers = papersForSection(index)
        return papers.count
    }
    
    func paperForItemAtIndexPath(indexPath: NSIndexPath) -> Paper? {
        if indexPath.section > 0 {
            let papers = papersForSection(indexPath.section)
            return papers[indexPath.item]
        } else {
            return papers[indexPath.item]
        }
    }
    
    // account
    func indexPathForAccount(account: RLMAccount) -> NSIndexPath {
        let section = self.accountSections.indexOf(account.section)
        var item = 0
        for (index, currentAccount) in accountsForSection(section!).enumerate() {
            if currentAccount === account {
                item = index
                break
            }
        }
        return NSIndexPath(forItem: item, inSection: section!)
    }
    
    func numberOfAccountsInSection(index: Int) -> Int {
        let accounts = accountsForSection(index)
        return accounts.count
    }
    
    func accountForItemAtIndexPath(indexPath: NSIndexPath) -> RLMAccount? {
        if indexPath.section > 0 {
            let accounts = accountsForSection(indexPath.section)
            return accounts[indexPath.item]
        } else {
            return accounts[indexPath.item]
        }
    }
    
    func titleForSectionAtIndexPath(indexPath: NSIndexPath) -> String? {
//        if indexPath.section < sections.count {
//            return sections[indexPath.section]
//        }
        
        if indexPath.section < self.accountSections.count {
            return self.accountSections[indexPath.section]
        }
        return nil
    }
    
    // MARK: Private
    private func loadPapersFromDisk() -> [Paper] {
        sections.removeAll(keepCapacity: false)
        if let path = NSBundle.mainBundle().pathForResource("Papers", ofType: "plist") {
            
            if let dictArray = NSArray(contentsOfFile: path) {
                var papers: [Paper] = []
                for item in dictArray {
                    if let dict = item as? NSDictionary {
                        let caption = dict["caption"] as! String
                        let imageName = dict["imageName"]as! String
                        let section = dict["section"] as! String
                        let index = dict["index"] as! Int
                        let paper = Paper(caption: caption, imageName: imageName, section: section, index: index)
                    
                        if !sections.contains(section) {
                            sections.append(section)
                        }
                        papers.append(paper)
                    }
                }
                return papers
            }
        }
        return []
    }
    
    private func loadAccountsFromRealm() -> [RLMAccount] {
        self.accountSections.removeAll()
        
        let userDefault = NSUserDefaults.standardUserDefaults()
        let tmpId = (userDefault.objectForKey("bookIdCount") as? Int)!
        let accountBookId = String(tmpId)
//        var accountBook = RLMAccountBook()
        var accounts: [RLMAccount] = []

        let realm = try! Realm()
//        let resultAccountBook = realm.objects(RLMAccountBook).filter("id == '\(accountBookId)'")
//        accountBook = resultAccountBook[0]
        
        let resultAccount = realm.objects(RLMAccount).filter("accountBookId == '\(accountBookId)'")
        
        // 從帳簿結果裡面 撈出每個帳目 然後塞到 rlmAccount modle 裡面
        for (index, item) in resultAccount.enumerate() {
            print("==\(index)==")
//            accounts.append(item)
            accounts.insert(item, atIndex: 0)
            
            // 判斷Section數
            if !accountSections.contains(item.travelDay) { // 拿日期來區分section
//                accountSections.append(item.travelDay)
                accountSections.insert(item.travelDay, atIndex: 0)
            }
        }
        
        return accounts
    }
    
    private func papersForSection(index: Int) -> [Paper] {
        let section = sections[index]
        let papersInSection = papers.filter { (paper: Paper) -> Bool in
            return paper.section == section
        }
        return papersInSection
    }
    
    // account
    private func accountsForSection(index: Int) -> [RLMAccount] {
        let section = self.accountSections[index]
        let accountsInSection = self.accounts.filter { (account: RLMAccount) -> Bool in
            return account.travelDay == section
        }
        return accountsInSection
    }
}
