//
//  TERPresettingView.swift
//  TravelExchangeRate
//
//  Created by Jaxun on 2015/10/7.
//  Copyright © 2015年 Jaxun. All rights reserved.
//

import UIKit
import PKRevealController
import Alamofire
import SwiftyJSON
import ReachabilitySwift
import RealmSwift

class TERPresettingView: UIViewController, PKRevealing, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    var accountBookId = "" // 帳本id
    let realm = try! Realm()
    @IBOutlet var accountBookPhotoView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.rateDisplay.delegate = self // for keyboard hide
        self.totalMoneyDisplay.delegate = self // for keyboard hide
        self.travelNameDisplay.delegate = self
        
        self.foreignRateListSetting()
        /**
        *  Swift Reachability
        */

        // 先判斷網路是否連線
        let reachability = Reachability.reachabilityForInternetConnection()
        if let reachability = reachability {
            if reachability.isReachable() {
                
                // 已連線 取得匯率
                //getExchangeRate("JPY")
            } else {
                
                // 未連線 show Alert
                showAlert("目前無網路")
            }
        }
        
        // Blur Effect View
        //        let blur: UIBlurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
        let blur = UIBlurEffect(style: .Light)
        let effectView = UIVisualEffectView(effect: blur)
        effectView.alpha = 0.8
        effectView.frame = self.view.frame
        
//        self.accountBookPhotoView.alpha = 0.8
        self.accountBookPhotoView .addSubview(effectView)
    }
    
    override func viewWillAppear(animated: Bool) {
        
        // 從userDefault 取出目前設定的匯率合總金額
        let userDefault = NSUserDefaults.standardUserDefaults()
        
        if userDefault.objectForKey("bookIdCount") != nil {
            let tmpId = (userDefault.objectForKey("bookIdCount") as? Int)!
            self.accountBookId = String(tmpId)
            
            var accountBook = RLMAccountBook()
            //let realm = try! Realm()
            let resultAccountBook = self.realm.objects(RLMAccountBook).filter("id == '\(self.accountBookId)'")
            
            // 第一次進來的會沒result
            if resultAccountBook.isEmpty == false {
                accountBook = resultAccountBook[0]
                
                let foreignCountryName = accountBook.countryName
                self.rateDisplay.text = accountBook.rate
                print("\(self.rateDisplay.text) % \(accountBook.rate)")
                self.totalMoneyDisplay.text = accountBook.allMoney
                self.foreignCountryBtn.setTitle(foreignCountryName, forState: UIControlState.Normal)
                
                let accountPhoto = accountBook.accountPhoto
                if accountPhoto != nil {
                    self.accountBookPhotoView.image = UIImage(data: accountBook.accountPhoto!)
                }
            }
        }
        
//        if userDefault.objectForKey("exchangeRate") != nil {
//            self.rateDisplay.text = userDefault.objectForKey("exchangeRate") as? String
//        }
//        if userDefault.objectForKey("totalMoney") != nil {
//            self.totalMoneyDisplay.text = userDefault.objectForKey("totalMoney") as? String
//        }
        
        // MARK: 看目前旅行狀態
        if userDefault.objectForKey("travelStatus") != nil {
         let travelStatus = userDefault.objectForKey("travelStatus") as? String
            // 旅行中, 匯率 ＆ 旅行國家 無法修改
            if travelStatus == "Traveling" {
                self.foreignCountryBtn.userInteractionEnabled = false
                self.rateDisplay.userInteractionEnabled = false
            }
            // 旅行後, 匯率 ＆ 旅行國家 & 兌換金額 無法修改
            if travelStatus == "AfterTravel" {
                self.rateDisplay.userInteractionEnabled = false
                self.travelNameDisplay.userInteractionEnabled = false
                self.totalMoneyDisplay.userInteractionEnabled = false
            }
        }
        
    }
    
    // MARK: 用Alamofire 取代 AFNetWorking 做HTTPS請求
    private func getExchangeRate(countryId: String) {

        // URL Request use "Alamofire"
        Alamofire.request(.GET, "https://asper-bot-rates.appspot.com/currency.json")
            .responseJSON { response in
                print(response)
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                }
        }
        
        Alamofire.request(.GET, "https://asper-bot-rates.appspot.com/currency.json?\(countryId)", parameters: nil)
            .validate()
            .responseJSON { response in
                
                print(response.request)  // original URL request
                print(response.response) // URL response
                print(response.data)     // server data
                print(response.result)   // result of response serialization
                
                switch response.result {
                case .Success:
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                    }
                    
                    /// MARK: 用SwiftJSON 取代 Mantle 做JSON解析
                    let rates = JSON(response.result.value!)["rates"]
                    
                    let buyCash = rates["buyCash"].stringValue
                    let buySpot = rates["buySpot"].stringValue
                    let sellCash = rates["sellCash"].stringValue
                    let sellSpot = rates["sellSpot"].stringValue
                    
                    print("================================")
                    print("buyCash = \(buyCash), buySpot = \(buySpot), sellCash = \(sellCash), sellSpot = \(sellSpot)")
                    print("================================")
                    
                    self.rateDisplay.text = buyCash
                case .Failure(let error):
                    print(error)
                    
                    // show alert! say error
                    self.rateDisplay.text = "0"
                }
        }
    }

    
    var maskView: UIView!
    var foreignRateListPickerView: TERForeignListPickerView!
    var foreignRateDataSource = ["美元","歐元","港幣"]
    var chosedCountry: String!
    
    let countryIdDic = ["美元": "USD", "歐元": "EUR", "港幣": "HKD", "日圓": "JPY"]
    
    func foreignRateListSetting() {

        
        let views : NSArray = UINib(nibName: "TERForeignListPicker", bundle: nil).instantiateWithOwner(self, options: nil)
        
        //從xib建立一個views
        self.foreignRateListPickerView = views.objectAtIndex(0) as! TERForeignListPickerView
        self.foreignRateListPickerView.myPickerView.delegate = self
        self.maskView = UIView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height))
        self.maskView.backgroundColor = UIColor.blackColor()
        self.maskView.alpha = 0
        let select: Selector = "hideForeignPicker:"
        
        // add action
        self.foreignRateListPickerView.OK.addTarget(self, action: select, forControlEvents: UIControlEvents.TouchUpInside)
        let gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: select)
        self.maskView.addGestureRecognizer(gesture)
    }
    
    // MARK: PickerView顯示欄數
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    // MARK: PickerView顯示行數
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        self.chosedCountry = foreignRateDataSource[0] // 一開始先給訂第一個值
        return  foreignRateDataSource.count
    }
    // MARK: PickerView display content
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return foreignRateDataSource[row]
    }
    // MARK: Chosen the Country Name
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.chosedCountry = foreignRateDataSource[row]
        
    }
    // MARK: Show Picker View
    func showForeignRateListPickerView() {
        self.view.addSubview(self.maskView)
        self.view.addSubview(self.foreignRateListPickerView)
        
        self.maskView.alpha = 0
        self.foreignRateListPickerView.frame.origin.y = self.view.frame.height
        self.foreignRateListPickerView.bounds = CGRectMake(0, self.foreignRateListPickerView.bounds.origin.y, UIScreen.mainScreen().bounds.width, self.foreignRateListPickerView.bounds.height)
        self.foreignRateListPickerView.frame.origin.x = 0
        
        // Animaiton effect
        UIView.animateWithDuration(0.3) { () -> Void in
            self.maskView.alpha = 0.3
            print(self.foreignRateListPickerView.frame)
            self.foreignRateListPickerView.frame.origin.y = self.view.frame.height - self.foreignRateListPickerView.frame.height
            print(self.foreignRateListPickerView.frame)
        }
    }
    
    @IBOutlet weak var totalMoneyDisplay: UITextField!
    @IBOutlet weak var rateDisplay: UITextField!
    @IBOutlet weak var travelNameDisplay: UITextField!
    
    // MARK: UITextField Delegate
     func textFieldDidBeginEditing(textField: UITextField) {
        if textField .isEqual(totalMoneyDisplay) {
            
            if totalMoneyDisplay.text == "0" {
                totalMoneyDisplay.text = ""
            }
            
            // 輸入欄位上移
            UIView.animateWithDuration(0.3, animations: { () -> Void in
                self.view.frame.origin.y -= 150
            })
        }
        if textField .isEqual(travelNameDisplay) {
            
            travelNameDisplay.text = ""

            
            // 輸入欄位上移
            UIView.animateWithDuration(0.3, animations: { () -> Void in
                self.view.frame.origin.y -= 150
            })
        }
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if textField .isEqual(totalMoneyDisplay) || textField .isEqual(travelNameDisplay) {
            self.view.frame.origin.y += 150
        }
        
        // check first value is not 0.
        
        let tmpTotalNum = textField.text!
        if tmpTotalNum.hasPrefix("0") {
            
            //  輸入金額錯誤show Alert
            self.showAlert("請輸入正確金額！")
            
            self.totalMoneyDisplay.text = ""
        }
        
        // 帳本命名判斷
        if textField .isEqual(travelNameDisplay) {
            
            if travelNameDisplay.text! .isEmpty {
                //  命名不可為空 show Alert
                showAlert("命名不可為空！")
                
                self.travelNameDisplay.text = "請命名！"
            }
            /*
            if travelNameDisplay.text! .isEmpty || userDefault.objectForKey(travelNameDisplay.text!) != nil
            {
                //  命名有重複 show Alert
                showAlert("命名已重複！")
                
                self.travelNameDisplay.text = ""
                
            } else {
                // 設定命名, DataBase也是以此命名
                userDefault.setObject(travelNameDisplay.text!, forKey: travelNameDisplay.text!)
            } */
        }
    }
    
    // 點擊畫面時,關閉輸入鍵盤
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.totalMoneyDisplay.resignFirstResponder()
        self.rateDisplay.resignFirstResponder()
        self.travelNameDisplay.resignFirstResponder()
    }
    
    @IBOutlet weak var foreignCountryBtn: UIButton!
    @IBAction func hideForeignPicker(sender:AnyObject)
    {
        self.hideForeignPicker()
        
        // catch key to parse api
        let countryId = self.countryIdDic[self.chosedCountry]
        
        // display rate number
        self.getExchangeRate(countryId!)
        
        // MARK: update some interface
        self.foreignCountryBtn.setTitle(self.chosedCountry, forState: UIControlState.Normal)
    }
    
    func hideForeignPicker(){
        UIView.animateWithDuration(0.3,
            animations:
            {
                self.maskView.alpha = 0;
                self.foreignRateListPickerView.frame.origin.y = self.view.frame.height
            },
            completion:
            {
                (value:Bool) in
                
                self.maskView.removeFromSuperview()
                self.foreignRateListPickerView.removeFromSuperview()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func clickForeignRateBtn(sender: UIButton) {
        self.showForeignRateListPickerView()
    }
    
    private func checkValueThenCreate() -> Bool {
        
        // 檢查輸入金額
        // 輸入總金額錯誤取消跳頁
        let tmpTotalNum = self.totalMoneyDisplay.text!
        let tmpRate = self.rateDisplay.text!
        let tmpName = self.travelNameDisplay.text!
        
        if tmpTotalNum.hasPrefix("0") ||
            tmpTotalNum.isEmpty
        {
            //  輸入金額錯誤show Alert
            showAlert("請輸入正確金額！")
            self.totalMoneyDisplay.text = ""
            
            return false
        }
        
        // 檢查匯率
        else if tmpRate.hasPrefix("0") ||
            tmpRate.isEmpty
        {
            //  輸入金額錯誤show Alert
            showAlert("請輸入或選擇匯率！")
            
            return false
        }
        
        // 檢查名稱
        else if tmpName.isEmpty
        {
            //  輸入金額錯誤show Alert
            showAlert("請輸入帳本名稱！")
            
            return false
        } else {
            
            // 都沒問題再建或更新資料庫
            self.creatAccountBook()
            return true
        }
    }
    
    // MARK: 創建帳本
    private func creatAccountBook() {
        let userDefault = NSUserDefaults.standardUserDefaults()
        
        let newAccountBook = RLMAccountBook()
        newAccountBook.travelName = self.travelNameDisplay.text!
        newAccountBook.rate = self.rateDisplay.text!
        newAccountBook.allMoney = self.totalMoneyDisplay.text!
        newAccountBook.countryName = (self.foreignCountryBtn.titleLabel?.text)!
        if self.accountBookPhotoView.image != nil {
            newAccountBook.accountPhoto = UIImageJPEGRepresentation(self.accountBookPhotoView.image!, 50)!
        }
        
        var idCount = 0
        if userDefault.objectForKey("bookIdCount") != nil {
            idCount = (userDefault.objectForKey("bookIdCount") as? Int)!
        }
        
        // 如果是旅行中 ＆ 旅行後, 就算是修改。 id 不用+1
        if userDefault.objectForKey("travelStatus") != nil {
            let travelStatus = userDefault.objectForKey("travelStatus") as? String
            // 出發前才要
            if travelStatus == "BeforeTravel" {
                idCount += 1
            }
        }
        
        self.accountBookId = String(idCount)
        newAccountBook.travelStatus = userDefault.objectForKey("travelStatus") as! String
        newAccountBook.id = self.accountBookId  // 記住id 來做增加帳目的依據
        
        userDefault.setObject(idCount, forKey: "bookIdCount")
        
        // 創建帳本
        
        self.realm.beginWrite()
//            try! realm.write {
                self.realm.add(newAccountBook, update: true)
//            }
    }
    
    @IBAction func backToMainView(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func clickSave(sender: UIButton) {
        if checkValueThenCreate() == true {
            try! self.realm.commitWrite()
            
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    @IBAction func clickGo(sender: UIButton) {
        // 旅行後不按出發旅行
        let userDefault = NSUserDefaults.standardUserDefaults()
        
        if userDefault.objectForKey("travelStatus") != nil {
            let travelStatus = userDefault.objectForKey("travelStatus") as? String
            // 旅行後, 匯率 ＆ 旅行國家 & 兌換金額 無法修改
            if travelStatus == "AfterTravel" {
                
                // 推進 統計頁面＋側邊欄
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let calculaterVC = storyboard.instantiateViewControllerWithIdentifier("AccountList") as! TERAccountList
                let sideMenuVC   = storyboard.instantiateViewControllerWithIdentifier("SideMenu") as! TERSideMenu
                
                let revealController = PKRevealController(frontViewController: calculaterVC, leftViewController: sideMenuVC)
                
                self.presentViewController(revealController, animated: false, completion: nil)
                
                return
            }
        }
        
        if checkValueThenCreate() == true {
            
            // 推進 計算機頁面＋側邊頁面
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let calculaterVC = storyboard.instantiateViewControllerWithIdentifier("CalculaterView") as! TERCalculaterView
            let sideMenuVC   = storyboard.instantiateViewControllerWithIdentifier("SideMenu") as! TERSideMenu
            
            let revealController = PKRevealController(frontViewController: calculaterVC, leftViewController: sideMenuVC)
            
            self.presentViewController(revealController, animated: false, completion: nil)
            
            // 把匯率, 剩餘金額 帶到計算頁面
            var tmpRate = Double(self.rateDisplay.text!)
            // 取小數點後兩位計算
            tmpRate = round(tmpRate! * 100) / 100
            calculaterVC.exchangeRate = tmpRate
            
            // 存匯率值
            userDefault.setObject(tmpRate, forKey:"exchangeRate")
            
            // 全部消費金額
            var tsm = "0"
            if userDefault.objectForKey("totalSpendMoney") != nil {
                tsm = userDefault.objectForKey("totalSpendMoney") as! String
            }
            
            let totalMoneyInt = Int(self.totalMoneyDisplay.text!)
            let totalSpendMoneyInt = Int(tsm)
            let lmfString = String(totalMoneyInt! - totalSpendMoneyInt!)
            calculaterVC.lastMoneyField.text! = lmfString
            
            // 把匯率帶到側邊欄
            sideMenuVC.totalMoneyDisplay.text = self.totalMoneyDisplay.text
            // 把帳本名稱帶到側邊欄
            sideMenuVC.travelNameDisplay.text = self.travelNameDisplay.text
            
            // 存總金額
            userDefault.setObject(self.totalMoneyDisplay.text, forKey: "totalMoney")
            
            // 設定為旅行中狀態
            let travelStatus = AccountStatus(rawValue: "Traveling")
            
            print("now is \(travelStatus)")
            userDefault.setObject(travelStatus!.rawValue, forKey: "travelStatus")
            
            userDefault.synchronize()
            
            // 旅行狀態更新到Realm
//            let tmpId = (userDefault.objectForKey("bookIdCount") as? Int)!
//            let accountBookId = String(tmpId)
//            let travelStatus = (userDefault.objectForKey("travelStatus"))!
            
            var accountBook = RLMAccountBook()
            let realm = try! Realm()
            let resultAccountBook = realm.objects(RLMAccountBook).filter("id == '\(accountBookId)'")
            accountBook = resultAccountBook[0]
            accountBook.travelStatus = String(travelStatus!)
            if self.accountBookPhotoView.image != nil {
                accountBook.accountPhoto = UIImageJPEGRepresentation(self.accountBookPhotoView.image!, 50)
            }
            self.realm.add(accountBook, update: true) // 以帳目為主
            try! self.realm.commitWrite()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "PresettingToCalculater" {
//            let calculateVC = segue.destinationViewController as! TERCalculaterView
//            calculateVC.exchangeRate = 0.5
        }
    }
    
    func showAlert(title: String) {
        //  輸入金額錯誤show Alert
        
        let alert: UIAlertController =
        UIAlertController(title: title, message: nil, preferredStyle: .Alert)
        let okAction: UIAlertAction = UIAlertAction(title: "OK", style: .Default) { action -> Void in }
        alert.addAction(okAction)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
}


extension TERPresettingView: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBAction func setAccountBookPhoto (sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
            
            // Chose camara or album
            
            let alert: UIAlertController =
            UIAlertController(title: "error", message: nil, preferredStyle: .Alert)
            
            let camaraAction: UIAlertAction = UIAlertAction(title: "open camara", style: .Default) { action -> Void in
                
                let imgPicker = UIImagePickerController()
                imgPicker.delegate = self
                imgPicker.sourceType = UIImagePickerControllerSourceType.Camera;
                imgPicker.allowsEditing = true
                //            imag.mediaTypes = [kUTTypeImage]
                
                self.presentViewController(imgPicker, animated: true, completion: nil)
            }
            let albumAction: UIAlertAction = UIAlertAction(title: "open from album", style: .Default) { action -> Void in
                // Open album
                self.openFromAlbum()
            }
            alert.addAction(camaraAction)
            alert.addAction(albumAction)
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            // Open album
            self.openFromAlbum()
        }
    }
    func openFromAlbum() {
        let imgPicker = UIImagePickerController()
        imgPicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        imgPicker.delegate = self
        
        self.presentViewController(imgPicker, animated: true, completion: nil)
    }
    
    // MARK: - Camara Delegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        var image: UIImage!

        // fetch the selected image
        if picker.allowsEditing {
            image = info[UIImagePickerControllerEditedImage] as! UIImage
        } else {
            image = info[UIImagePickerControllerOriginalImage] as! UIImage
        }
        
        if self.accountBookPhotoView.image != nil {
        self.accountBookPhotoView.image = nil
        }
        
        // 拍照模式才需要存照片
        if picker.sourceType == UIImagePickerControllerSourceType.Camera {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
        delay(0.2) { () -> () in
            self.dismissViewControllerAnimated(true, completion: { () -> Void in
                self.accountBookPhotoView.image = image
            })
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
