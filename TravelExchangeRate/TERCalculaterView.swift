//
//  TERCalculaterView.swift
//  TravelExchangeRate
//
//  Created by Jaxun on 2015/10/7.
//  Copyright © 2015年 Jaxun. All rights reserved.
//

import UIKit

// Basic math functions
func add (a: Double, b: Double) -> Double {
    let result = a + b
    return result
}

func sub (a: Double, b: Double) -> Double {
    let result = a - b
    return result
}

func mul (a: Double, b: Double) -> Double {
    let result = a * b
    return result
}

func div (a: Double, b: Double) -> Double {
    let result = a / b
    return result
}

typealias Binop = (Double, Double) -> Double
let ops: [String: Binop] = ["+": add, "-": sub, "*": mul, "/": div]

class TERCalculaterView: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewWillAppear(animated: Bool) {
        // 從userDefault 取出目前設定的匯率合總金額  (從Realm 的匯率)
        let userDefault = NSUserDefaults.standardUserDefaults()
        
        if userDefault.objectForKey("currentMoneyDisplay") != nil {
            self.lastMoneyField.text = userDefault.objectForKey("currentMoneyDisplay") as? String
        }
    }
    var exchangeRate: Double!
    var accumulator: Double = 0.0 // Store the calculated value here.
    var userInput = "" // User-enterd digits
    
    var numStack: [Double] = [] // Number stack
    var opStack: [String] = [] // Operator stack
    
    
    // Looks for a single charactor in a string.
    func hasIndex(stringToSearch str: String, characterToFind chr: Character) -> Bool {
        for c in str.characters {
            if c == chr {
                return true
            }
        }
        return false
    }
    
    func handleInput(str: String) {
        if str == "-" {
            if userInput.hasPrefix(str) {
                // Strip off the first character (a dash)
                userInput = userInput.substringFromIndex(userInput.startIndex.successor())
            } else {
                userInput = str + userInput
            }
        } else {
            userInput += str
        }
        accumulator = Double((userInput as NSString).doubleValue)
        updateDisplay()
    }
    
    func updateDisplay() {
        // If the value is an integer, don't show a decimal
        let userDefault = NSUserDefaults.standardUserDefaults()
        
        if userDefault.objectForKey("exchangeRate") != nil {
            exchangeRate = userDefault.objectForKey("exchangeRate") as! Double
        }
        let iAcc = Int(accumulator)
        
        /// MARK: 匯率轉換方式
        let exchangeNum = accumulator * exchangeRate // from foreign to local
        if accumulator - Double(iAcc) == 0 {
            numField.text = "\(iAcc)"
        } else {
            numField.text = "\(accumulator)"
        }
        exchangeField.text = "\(exchangeNum)"
    }
    
    func doMath(newOp: String) {
        if userInput != "" && !numStack.isEmpty {
            let stackOp = opStack.last
            if !((stackOp == "+" || stackOp == "-") &&
                (newOp == "*" || newOp == "/")) {
                    let oper = ops[opStack.removeLast()]
                    accumulator = oper!(numStack.removeLast(), accumulator)
                    doEquals()
            }
        }
        opStack.append(newOp)
        numStack.append(accumulator)
        userInput = ""
        updateDisplay()
    }
    
    func doEquals() {
        if userInput == ""  {
            return
        }

        let stackOp = opStack.last
        if stackOp == "/" && accumulator == 0 {
            
            let alert: UIAlertController =
            UIAlertController(title: "error", message: nil, preferredStyle: .Alert)
            
            let okAction: UIAlertAction = UIAlertAction(title: "OK", style: .Default) { action -> Void in }
            alert.addAction(okAction)
            self.presentViewController(alert, animated: true, completion: nil)

            return
        }
        if !numStack.isEmpty {
            let oper = ops[opStack.removeLast()]
            accumulator = oper!(numStack.removeLast(),accumulator)
            if !opStack.isEmpty {
                doEquals()
            }
        }
        updateDisplay()
        userInput = ""
    }
    
    // UI Set-up
    @IBOutlet weak var numField: UILabel!
    @IBOutlet weak var exchangeField: UILabel!
    @IBOutlet weak var lastMoneyField: UILabel!
    
    
    @IBOutlet var btnClear: UIButton!
    @IBOutlet var bntEquals: UIButton!
    @IBOutlet var btnAdd: UIButton!
    @IBOutlet var btnSubtract: UIButton!
    @IBOutlet var btnMultiply: UIButton!
    @IBOutlet var btnDivide: UIButton!
    @IBOutlet var btnDecimal: UIButton!
    
    @IBOutlet var btn0: UIButton!
    @IBOutlet var btn1: UIButton!
    @IBOutlet var btn2: UIButton!
    @IBOutlet var btn3: UIButton!
    @IBOutlet var btn4: UIButton!
    @IBOutlet var btn5: UIButton!
    @IBOutlet var btn6: UIButton!
    @IBOutlet var btn7: UIButton!
    @IBOutlet var btn8: UIButton!
    @IBOutlet var btn9: UIButton!
    
    
    @IBAction func btn0Press(sender: UIButton) {
        handleInput("0")
    }
    @IBAction func btn1Press(sender: UIButton) {
        handleInput("1")
    }
    @IBAction func btn2Press(sender: UIButton) {
        handleInput("2")
    }
    @IBAction func btn3Press(sender: UIButton) {
        handleInput("3")
    }
    @IBAction func btn4Press(sender: UIButton) {
        handleInput("4")
    }
    @IBAction func btn5Press(sender: UIButton) {
        handleInput("5")
    }
    @IBAction func btn6Press(sender: UIButton) {
        handleInput("6")
    }
    @IBAction func btn7Press(sender: UIButton) {
        handleInput("7")
    }
    @IBAction func btn8Press(sender: UIButton) {
        handleInput("8")
    }
    @IBAction func btn9Press(sender: UIButton) {
        handleInput("9")
    }
    @IBAction func btnDecPress(sender: UIButton) {
        if hasIndex(stringToSearch: userInput, characterToFind: ".") ==
            false {
                handleInput(".")
        }
    }
    @IBAction func btnACPress(sender: UIButton) {
        userInput = ""
        accumulator = 0
        updateDisplay()
        numStack.removeAll()
        opStack.removeAll()
    }
    
    @IBAction func btnPlusPress(sender: UIButton) {
        doMath("+")
    }
    @IBAction func btnMinusPress(sender: UIButton) {
        doMath("-")
    }
    @IBAction func btnMultiplyPress(sender: UIButton) {
        doMath("*")
    }
    @IBAction func btnDividePress(sender: UIButton) {
        doMath("/")
    }
    @IBAction func btnEqualsPress(sender: UIButton) {
        doEquals()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // 點擊側邊menu按鈕
    @IBAction func clickSideMenuBtn(sender: UIButton) {
        if self.revealController != nil {
            self.revealController.showViewController(self.revealController.leftViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func addAccountItem(sender: UIButton) {
        self.performSegueWithIdentifier("CalculaterToKeyin", sender: self)
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "CalculaterToKeyin" {
            //get destination controller
            let keyinVC = segue.destinationViewController as! TERKeyinView

            keyinVC.moneyString = numField.text!
            keyinVC.exchangeMoneyString = exchangeField.text!
        }
    }
}


// Rate Calculate