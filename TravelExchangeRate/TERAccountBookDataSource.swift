//
//  TERAccountBookDataSource.swift
//  TravelExchangeRate
//
//  Created by Jaxun on 2015/12/30.
//  Copyright © 2015年 Jaxun. All rights reserved.
//


import UIKit
import RealmSwift

class TERAccountBookDataSource {
    

    private var sections: [String] = []
    private var accountBooks: [RLMAccountBook] = []
    
    var count: Int {
        return accountBooks.count
    }
    
    init() {
        
        // get account sections
        //###########
        accountBooks = loadAccountBooksFromRealm()
        //##########
    }

    func accountForItemAtIndexPath(indexPath: NSIndexPath) -> RLMAccountBook? {
        return accountBooks[indexPath.item]
    }
    
    func accountForItemAtIndex(index: Int) -> RLMAccountBook? {
        if index > 0 {
            return accountBooks[index - 1]
        } else {
            return accountBooks[index]
        }
        
    }

    // MARK: Private
    
    private func loadAccountBooksFromRealm() -> [RLMAccountBook] {
        
        var accountBooks: [RLMAccountBook] = []
        
        let realm = try! Realm()
        
        let resultAccount = realm.objects(RLMAccountBook)
        
        // 從帳簿結果裡面 撈出每個帳目 然後塞到 rlmAccount modle 裡面
        for (index, item) in resultAccount.enumerate() {
            print("==\(index)==")
                        accountBooks.append(item)
//            accountBooks.insert(item, atIndex: 0)
        }
        return accountBooks
    }
}

