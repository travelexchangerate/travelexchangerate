//
//  TERStatisticalView.swift
//  TravelExchangeRate
//
//  Created by Jaxun on 2015/10/12.
//  Copyright © 2015年 Jaxun. All rights reserved.
//

import UIKit
import RealmSwift

class TERStatisticalView: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backMainView(sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let accountListVC = storyboard.instantiateViewControllerWithIdentifier("MainView")
        self.presentViewController(accountListVC, animated: false, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "MainToPresetting" {
            //get destination controller
            //            var destViewController = segue.destinationViewController as! TERPresettingView;
            //            destViewController.receiveData = "SegueData!!!!!";
        }
    }
}


