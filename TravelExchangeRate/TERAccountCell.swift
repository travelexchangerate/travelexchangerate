//
//  TERAccountCell.swift
//  TravelExchangeRate
//
//  Created by Jaxun on 2015/11/27.
//  Copyright © 2015年 Jaxun. All rights reserved.
//

import UIKit

class PaperCell: UICollectionViewCell {
    
    @IBOutlet weak var paperImageView: UIImageView!
    @IBOutlet weak var gradientView: GradientView!
    @IBOutlet weak var captionLabel: UILabel!
    
    var paper: Paper? {
        didSet {
            if let paper = paper {
                paperImageView.image = UIImage(named: paper.imageName)
                captionLabel.text = paper.caption
            }
        }
    }
    
    var account: RLMAccount? {
        didSet {
//            if let account = RLMAccount {
            let photoImg = UIImage(data: (account?.photo1)!)
            print("\(photoImg)")
//            paperImageView.image = photoImg
            
            captionLabel.text = account?.accountName
            print("\(captionLabel.text)")
//            }
        }
    }
    
    var accountBook: RLMAccountBook? {
        didSet {
            let photoImg = UIImage(data: (accountBook?.accountPhoto)!)
            print("\(photoImg)")

            captionLabel.text = accountBook?.travelName
            print("\(captionLabel.text)")
        }
    }
}

