 //
//  AppDelegate.swift
//  TravelExchangeRate
//
//  Created by Jaxun on 2015/10/7.
//  Copyright © 2015年 Jaxun. All rights reserved.
//

import UIKit
 
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
//        delay(0.3) { () -> () in
//                    NSNotificationCenter.defaultCenter().postNotificationName("CheckTravelStatus", object: self, userInfo: nil)
//        }

        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
    }

    func applicationWillEnterForeground(application: UIApplication) {
        delay(0) { () -> () in
            NSNotificationCenter.defaultCenter().postNotificationName("CheckTravelStatus", object: self, userInfo: nil)
        }
    }

    func applicationDidBecomeActive(application: UIApplication) {
    }

    func applicationWillTerminate(application: UIApplication) {
    }
}

