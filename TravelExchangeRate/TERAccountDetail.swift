//
//  TERAccountDetail.swift
//  TravelExchangeRate
//
//  Created by Jaxun on 2015/10/8.
//  Copyright © 2015年 Jaxun. All rights reserved.
//

import UIKit
import iCarousel
import Cosmos

class TERAccountDetail: UIViewController {
    
    @IBOutlet var mockImgV: UIImageView!
    @IBOutlet var likeRate: CosmosView!
    
    var photo1: UIImage!
    var photo2: UIImage!
    var photo3: UIImage!
    var photo4: UIImage!
    
    var account: RLMAccount!
    
    @IBOutlet var carousel: iCarousel!
    
    @IBOutlet var photoScrollView: UIScrollView!

    var photoImages: [UIImage] = []
    override func viewWillAppear(animated: Bool) {
        print("\(account)")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("\(photo1)")
        print("\(account)")
//        photoImages = [UIImage(named:"photo1.png")!,
//            UIImage(named:"photo2.png")!,
//            UIImage(named:"photo3.png")!,
//            UIImage(named:"photo4.png")!,
//            UIImage(named:"photo5.png")!]

//        let photoScrollViewSize = self.photoScrollView.frame.size
        
//        self.photoScrollView.contentSize = CGSizeMake(photoScrollViewSize.width * CGFloat(photoImages.count), photoScrollViewSize.height)
        
        self.carousel.type = .CoverFlow2
     
        self.carousel.frame.size.width = UIScreen.mainScreen().bounds.width
        self.likeRate.userInteractionEnabled = false
        self.likeRate.rating = Double(account.feelLevel)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backToMenu(sender: UIButton) {
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    @IBAction func addAccountBook(sender: UIButton) {
        self.performSegueWithIdentifier("AccountListToAccountDetail", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "AccountListToAccountDetail" {
            //get destination controller
            //            var destViewController = segue.destinationViewController as! TERPresettingView;
            //            destViewController.receiveData = "SegueData!!!!!";
        }
    }
}

extension TERAccountDetail: iCarouselDataSource, iCarouselDelegate {

    
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int
    {
        /// MARK:純
        if UIImage(data: self.account.photo1!) != nil {
            self.photoImages.append(UIImage(data: self.account.photo1!)!)
        }
        if UIImage(data: self.account.photo2!) != nil {
            self.photoImages.append(UIImage(data: self.account.photo2!)!)
        }
        if UIImage(data: self.account.photo3!) != nil {
            self.photoImages.append(UIImage(data: self.account.photo3!)!)
        }
        if UIImage(data: self.account.photo4!) != nil {
            self.photoImages.append(UIImage(data: self.account.photo4!)!)
        }
        
        
        return self.photoImages.count
    }
    
    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int, reusingView view: UIView?) -> UIView
    {
        var photoView: UIImageView
        
        photoView = UIImageView(frame:CGRect(x:0, y:0, width:UIScreen.mainScreen().bounds.width, height:200))

        let changeSize: CGSize = photoView.frame.size
        
        var img = UIImage(data: self.account.photo1!)
        switch index {
        case 0:
            img = UIImage(data: self.account.photo1!)
        case 1:
            img = UIImage(data: self.account.photo2!)
        case 2:
            img = UIImage(data: self.account.photo3!)
        case 3:
            img = UIImage(data: self.account.photo4!)
        default:
            photoView.image = photo4
        }

        if img == nil {
            img = UIImage(named: "Checked")
        }
        let finalImg = imageResize(img!, sizeChange: changeSize)
        photoView.image = finalImg
        
        return photoView
    }
    
    func imageResize (imageObj:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = false
        let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        imageObj.drawInRect(CGRect(origin: CGPointZero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage
    }
}

