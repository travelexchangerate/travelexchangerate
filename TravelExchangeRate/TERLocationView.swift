//
//  TERLocationView.swift
//  TravelExchangeRate
//
//  Created by Jaxun on 2015/12/2.
//  Copyright © 2015年 Jaxun. All rights reserved.
//

import UIKit
import MapKit
import MobileCoreServices
import AssetsLibrary
import Photos

class TERLocationView: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    // MARK: Property
    let locationManager = CLLocationManager()
    var initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
    let mockLocation = CLLocation(latitude: 0.0, longitude: 0.0)
    let regionRadius: CLLocationDistance = 1000
    var pointAnnotation:MKPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    
    weak var keyinVC: TERKeyinView?

    // MARK: IBOutlet
    @IBOutlet weak var photoImgView: UIImageView!
    @IBOutlet weak var mapView: MKMapView!

    // MARK: IBAction
    
    // 更新 目前位置
    @IBAction func getCurrentLocation(sender: UIButton) {
        
        // MARK: 補上進來前先開定位確保有找到座標
        self.mapView.removeAnnotation(self.pointAnnotation)
        self.pointAnnotation = nil
        
        locationManager.startUpdatingLocation()
    }
    
    // 開相簿 選其照片中的地理位置
    @IBAction func getPhotoLocation(sender: UIButton) {
        let imgPicker = UIImagePickerController()
        imgPicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        imgPicker.delegate = self
        
        self.presentViewController(imgPicker, animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.requestWhenInUseAuthorization()
        self.mapView.delegate = self

        // 偵測定位已開
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            
            let userLocation = MKUserLocation()
            print("Location:\(userLocation.location)")
            locationManager.distanceFilter = kCLDistanceFilterNone
            locationManager.startUpdatingLocation()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backToKeyin(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        // 把取得的經緯度座標傳回KeyinView
        self.keyinVC?.location = self.pointAnnotation.coordinate
        print("net yet: \(self.pointAnnotation.coordinate)")
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        self.mapView.setRegion(coordinateRegion, animated: true)
    }
}

// MARK: - Camara Delegate
extension TERLocationView {
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {

        var image: UIImage!
        
        // 找出"照片"和"經緯度"位置
        if picker.allowsEditing {
            image = info[UIImagePickerControllerEditedImage] as! UIImage
        } else {
            image = info[UIImagePickerControllerOriginalImage] as! UIImage
        }
        let refURL = info[UIImagePickerControllerReferenceURL] as! NSURL
        let fetResult = PHAsset.fetchAssetsWithALAssetURLs([refURL], options: nil)
        let asset = fetResult.firstObject as? PHAsset
        
        // 更新座標點
        if (asset != nil) {
            self.updateAnnotationLocation(asset!)
        }
        
        // 更新照片
        let getPhotoImg = image
        self.photoImgView.image = getPhotoImg
        
        // 關閉相機畫面
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func updateAnnotationLocation(asset: PHAsset) {
        asset.creationDate?.description
        asset.location?.description
        let photoLocation = asset.location?.coordinate
        if photoLocation != nil {
            self.mapView.removeAnnotation(self.pointAnnotation)
            
            // 要延遲才不會crash!
            delay(0.4, closure: { () -> () in
                self.pointAnnotation.coordinate = photoLocation!
                self.mapView.addAnnotation(self.pointAnnotation)
                
                self.initialLocation = CLLocation(latitude: photoLocation!.latitude, longitude: photoLocation!.longitude)
                self.centerMapOnLocation(self.initialLocation)
            })
        } else {
            self.pointAnnotation.coordinate = mockLocation.coordinate
            self.mapView.addAnnotation(self.pointAnnotation) // 加一個假的座標 避免下一次因為remove 空的 annotation當掉
            
            // 要延遲才會show出來
            delay(0.4, closure: { () -> () in
                let alert: UIAlertController =
                UIAlertController(title: "此照片無座標紀錄！", message: nil, preferredStyle: .Alert)
                
                let okAction: UIAlertAction = UIAlertAction(title: "OK", style: .Default) { action -> Void in }
                alert.addAction(okAction)
                self.presentViewController(alert, animated: true, completion: nil)
            })
        }
    }

    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

// MARK: - CLLocationManagerDelegate
extension TERLocationView: CLLocationManagerDelegate {
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue: CLLocationCoordinate2D = (manager.location?.coordinate)!
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        self.initialLocation = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        
        if(self.pointAnnotation == nil) {
            self.pointAnnotation = MKPointAnnotation()
            self.pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: locValue.latitude, longitude:     locValue.longitude)
            
            // 要有Title 才能 Drag
            self.pointAnnotation.title = "You Are Here :)"
            self.mapView.addAnnotation(self.pointAnnotation)
            
            centerMapOnLocation(self.initialLocation)
        }
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Errors: " + error.localizedDescription)
    }
}

//MARK:- View for Annotation - MKMapViewDelegate
extension TERLocationView: MKMapViewDelegate {
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKPointAnnotation {
            let pinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "myPin")
            
            pinAnnotationView.pinTintColor = UIColor.orangeColor()
            pinAnnotationView.draggable = true
            pinAnnotationView.canShowCallout = true
            pinAnnotationView.animatesDrop = true
            
            return pinAnnotationView
        }
        return nil
    }
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        print("now location = \(self.pointAnnotation.coordinate)")
    }
    
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, didChangeDragState newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
        switch (newState) {
        case .Ending, .Canceling:
            view.dragState = .None
            
            print("new location = \(self.pointAnnotation.coordinate)")
        default: break
        }
    }
}

func delay(delay:Double, closure:()->()) {
    dispatch_after(
        dispatch_time(
            DISPATCH_TIME_NOW,
            Int64(delay * Double(NSEC_PER_SEC))
        ),
        dispatch_get_main_queue(), closure)
}

