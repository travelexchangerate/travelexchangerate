//
//  TERAccountModle.swift
//  TravelExchangeRate
//
//  Created by Jaxun on 2015/11/25.
//  Copyright © 2015年 Jaxun. All rights reserved.
//

import UIKit
import RealmSwift

// 旅行狀態
enum AccountStatus: String {
    case BeforeTravel = "BeforeTravel"
    case Traveling = "Traveling"
    case AfterTravel = "AfterTravel"
}

class RLMAccountBook: Object {
    dynamic var travelName: String = ""     // 帳本名稱
    dynamic var countryName: String = ""    // 出國名稱
    dynamic var rate: String = ""           // 選擇匯率
    dynamic var allMoney: String = ""       // 總金額
    dynamic var lastMoney: String = ""      // 剩餘金額
    dynamic var travelStatus: String = ""   // 旅行狀態
    dynamic var accountPhoto: NSData? = nil // 帳本照片
    //    let accounts = List<RLMAccount>()
    dynamic var id = ""                      // 帳本 ID
    override static func primaryKey() -> String? {
        return "id"
    }
}

// 帳目細節
class RLMAccount: Object {
    dynamic var aId = ""                      // Account Id
    dynamic var spendOrIncome: String = "支出" // 支出/收入（預設為支出
    dynamic var spendType: String = ""      // 消費方式（現金/信用卡)
    dynamic var accountName: String = ""    // 帳目名稱
    dynamic var price:String = ""           // 金額
    dynamic var exPrice: String = ""        // 換匯金額
    dynamic var category: String = ""       // 種類(早餐/午餐/點心)
    dynamic var subCategory: String = ""    // 小分類(牛奶/奶茶/水)
    dynamic var travelDay: String = ""      // 日期
    dynamic var travelTime: String = ""     // 時間
    dynamic var locationLatitude: Double = 0.0     // 經度位置
    dynamic var locationLongitude: Double = 0.0    // 緯度位置
    dynamic var feelLevel: Int = 0      // 心情(1~5分)
    dynamic var ps: String = ""             // 註記內容
    dynamic var section: String = ""        // 欄位 (依照日期區隔)
    dynamic var accountImgPath: String = "" // 帳目圖片路徑
    dynamic var photo1: NSData? = nil // 支持可选值
    dynamic var photo2: NSData? = nil // 支持可选值
    dynamic var photo3: NSData? = nil // 支持可选值
    dynamic var photo4: NSData? = nil // 支持可选值
    
    // 不能用PrimaryKey的方式, PrimaryKey 是唯一值
//    override static func primaryKey() -> String? {
//        return "aId"
//    }
    
    dynamic var name: String = ""     // mock
    
    dynamic var owner: RLMAccountBook!
    dynamic var accountBookId = ""                      // 目前不會filter owner 所使用的替代方案
}

class Account {
    
    var travelName: String  // 帳本名稱
    var accountName: String // 帳目名稱
    var accountImgPath: String // 帳目圖片路徑
    var price:String    // 金額
    var exPrice: String // 換匯金額
    var travelDay: String   // 日期
    var travelTime: String  // 時間
    var location: String    // 經緯度位置
    var captureImg: String  // 照片
    var feelLevel: String   // 心情(1~5分)
    var ps:String // 註記內容
    var category:String  // 種類(早餐/午餐/點心)
    var section: String // 欄位
    
    init(travelName: String,
        accountName: String,
        accountImgPath: String,
        price: String,
        exPrice: String,
        travelDay: String,
        travelTime: String,
        location: String,
        captureImg: String,
        feelLevel: String,
        ps: String,
        category: String,
        section: String
        ) {
            
            self.travelName = travelName
            self.accountName = accountName
            self.accountImgPath = accountImgPath
            self.price = price
            self.exPrice = exPrice
            self.travelDay = travelDay
            self.travelTime = travelTime
            self.location = location
            self.captureImg = captureImg
            self.feelLevel = feelLevel
            self.ps = ps
            self.category = category
            self.section = section
    }
}


class Paper {
    
    var caption: String
    var imageName: String
    var section: String
    var index: Int
    
    init(caption: String, imageName:String, section: String, index: Int) {
        self.caption = caption
        self.imageName = imageName
        self.section = section
        self.index = index
    }
    
    convenience init(copying paper: Paper) {
        self.init(caption: paper.caption, imageName: paper.imageName, section: paper.section, index: paper.index)
    }
    
}
