//
//  TERForeignListPickerView.swift
//  TravelExchangeRate
//
//  Created by Jaxun on 2015/11/18.
//  Copyright © 2015年 Jaxun. All rights reserved.
//

import UIKit
/**
 *自定義UIPickerView 對應TERForeignListPicker.xib
 *
 */
class TERForeignListPickerView: UIView {
    
    @IBOutlet weak var OK: UIButton!//定義一個buttom
    @IBOutlet weak var myPickerView: UIPickerView!//定義一個UIPickerView
    
}
