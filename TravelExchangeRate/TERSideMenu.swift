//
//  TERSideMenu.swift
//  TravelExchangeRate
//
//  Created by Jaxun on 2015/10/8.
//  Copyright © 2015年 Jaxun. All rights reserved.
//

import UIKit
import PKRevealController
import RealmSwift

class TERSideMenu: UIViewController {
    
    var totalMoney: Double?

    // 帳目呈現
    @IBOutlet weak var totalMoneyDisplay: UILabel!
    @IBOutlet weak var totalSpendMoneyDisplay: UILabel!
    @IBOutlet weak var currentMoneyDisplay: UILabel!
    
    // 帳目呈現(換匯後)
    @IBOutlet weak var totalMoneyExchangeDisplay: UILabel!
    @IBOutlet weak var totalSpendMoneyExchangeDisplay: UILabel!
    @IBOutlet weak var currentMoneyExchangeDisplay: UILabel!

    //
    @IBOutlet weak var finishTravelBtn: UIButton!
    
    // 旅行名稱
    @IBOutlet weak var travelNameDisplay: UILabel!
    
    // 目前帳本
    var accountBook = RLMAccountBook()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(animated: Bool) {
        let userDefault = NSUserDefaults.standardUserDefaults()

        let tsm = userDefault.objectForKey("totalSpendMoney")
        let tm = userDefault.objectForKey("totalMoney")
        userDefault.objectForKey("currentMoneyDisplay")
        var travelStatus = userDefault.objectForKey("TravelStatus") as? String
        
        
        let tmpId = (userDefault.objectForKey("bookIdCount") as? Int)!
        let accountBookId = String(tmpId)
        // 目前旅行帳本資料
        let realm = try! Realm()
        let resultAccount = realm.objects(RLMAccountBook).filter("id == '\(accountBookId)'")
        for (index, item) in resultAccount.enumerate() {
//            item.travelStatus
            self.travelNameDisplay.text = item.travelName
            travelStatus = String(item.travelStatus)
        }
        
        // 旅行後, 完成履行按鈕隱藏
        if travelStatus == "AfterTravel" {
            self.finishTravelBtn.hidden = true
        } else {
            self.finishTravelBtn.hidden = false
        }

        /// !mock value!
        
        let tmString = tm as? String
        let tsmString = tsm as? String
        self.totalMoneyDisplay.text = tmString                     // 全部金額
        self.totalSpendMoneyDisplay.text = tsmString               // 全部花費金額
        let currentMoneyCalculate = Int(self.totalMoneyDisplay.text!)! - Int(self.totalSpendMoneyDisplay.text!)!
        self.currentMoneyDisplay.text = String(currentMoneyCalculate)   // 目前剩餘金額
        
        // 處理換匯
        let exchangeRate = userDefault.objectForKey("exchangeRate") as! Double
        
        let tmDouble = Double(tmString!)
        let tsmDouble = Double(tsmString!)
        let exchangeTm = tmDouble! * exchangeRate                              // 全部金額(換匯後)
        let exchangeTsm = tsmDouble! * exchangeRate                            // 全部花費金額(換匯後)
        let exchangeCurrentMoneyCalculate = exchangeTm - exchangeTsm // 目前剩餘金額
        
        self.totalMoneyExchangeDisplay.text = String(exchangeTm)
        self.totalSpendMoneyExchangeDisplay.text = String(exchangeTsm)
        self.currentMoneyExchangeDisplay.text = String(exchangeCurrentMoneyCalculate)
        
        userDefault.setObject(self.currentMoneyDisplay.text, forKey: "currentMoneyDisplay")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // 計算匯率頁面 (有側邊欄)
    @IBAction func backToCalculater() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let calculaterVC = storyboard.instantiateViewControllerWithIdentifier("CalculaterView")
        let sideMenuVC   = storyboard.instantiateViewControllerWithIdentifier("SideMenu")
        
        let revealController = PKRevealController(frontViewController: calculaterVC, leftViewController: sideMenuVC)
        
        self.presentViewController(revealController, animated: false, completion: nil)
    }
    
    // 查看帳目 (有側邊欄)
    @IBAction func checkAccountList(sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let accountListVC = storyboard.instantiateViewControllerWithIdentifier("AccountList")
        let sideMenuVC    = storyboard.instantiateViewControllerWithIdentifier("SideMenu")
        
        let revealController = PKRevealController(frontViewController: accountListVC, leftViewController: sideMenuVC)
        
        self.presentViewController(revealController, animated: false, completion: nil)
    }
    // 回帳本列表
    @IBAction func backMainView(sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let accountListVC = storyboard.instantiateViewControllerWithIdentifier("MainView")
        self.presentViewController(accountListVC, animated: false, completion: nil)
    }
    
    // 完成此次旅行帳本
    @IBAction func finishAccountBook(sender: UIButton) {
        
        // 旅行狀態調整
        let userDefault = NSUserDefaults.standardUserDefaults()
        
        // MARK: 設定為"旅行後"狀態
        let travelStatus = AccountStatus(rawValue: "AfterTravel")
        userDefault.setObject(travelStatus?.rawValue, forKey: "travelStatus")
        userDefault.setObject(0, forKey: "AccountId") // 帳目紀錄歸零

        // 旅行狀態更新到Realm
        let tmpAccountBookId = (userDefault.objectForKey("bookIdCount"))!
        let accountBookId = String(tmpAccountBookId)

        let realm = try! Realm()
        self.accountBook.travelStatus = String(travelStatus)
        realm.beginWrite()
        realm.create(RLMAccountBook.self, value: ["id": accountBookId, "travelStatus": "AfterTravel"], update: true)
        try! realm.commitWrite()
        
        userDefault.synchronize()
        
        // 到統計頁
         self.performSegueWithIdentifier("MenuToSatistical", sender: self)
    }
    
    
    // 修改兌換資料
    @IBAction func editExchangeRate(sender: UIButton) {
        
        // 設定編輯狀態
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let accountListVC = storyboard.instantiateViewControllerWithIdentifier("PresettingView")
        self.presentViewController(accountListVC, animated: false, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "MenuToSatistical" {
            
        }
    }
}